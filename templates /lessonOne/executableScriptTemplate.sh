#!/bin/bash 

###################################################################
#Script Name	:Bash Script Template                                                                                            
#Date Last Mod 	:Dec 20, 2010                                                                              
#Args           :                                                                                           
#Author       	:Ashleigh Carroll                                              
#Email         	:anzcarroll@gmail.com                                           
###################################################################
# 
# --------- Variables ---------- #
# myDirectory=`pwd`
STARTDIRECTORY=`pwd`
myDirectory='/Users/ashleighncarroll/'
# ------------------------------ #

# $myDirectory  ${myDirectory}          # these are the same things
# `command Here` $(command Here)        # these are the same thing as well, it's where everything inside 
                                        # the delimiter is actually executed

set -e                              # exit when command fails


# set -x                            # sets the script to consistently display all commands that are run

echo "script is starting now"       # * Announces when the script is starting

echo "script is ending now"         # * Announces when the script is ending


#Analyzes all the files in the current directory (whatever that directory is) 
# NEED TO COME BACK AND FIX THIS 
echo $myDirectory
# if $file in $path > 10 characters echo $file 


function readDirectories {
    CURRENTPATH=$1

    cd $CURRENTPATH
    echo "Current path for this iteration is $CURRENTPATH"
    for file in `ls`; do

        # print if >16chars
        characters=`echo "$file" | wc -c`; 
        # echo $characters
        if [[ $characters -ge 11 ]]  ;  then
            echo "$file has $characters characters"
        fi

        # check if its dir
        # FULLPATH=${CURRENTPATH}${file}
        echo "Checking to see if ${file} is directory"
        if [[ -d $file ]] ; then
            echo "$file is a directory"
            cd $file
            readDirectories
            cd ../
        fi


    done
} 



readDirectories $myDirectory

# If the current directory structure has other directories in it, 
# do the same thing for all the files (and directories) in the sub-directory