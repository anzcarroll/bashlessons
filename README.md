# Bash Script Template

This is a bash script template that does the following: 
* Invokes the right interpreter 
* exit when command fails
* Sets the script to consistently display all commands that are run
* Announces when the script is starting
* Announces when the script is ending
* Analyzes all the files in the current directory 
### expected variables:
CURRENTPATH
file 
characters 

## Installation

git clone to run local (make sure you have executable access)

```bash
/path/to/script.sh
```
